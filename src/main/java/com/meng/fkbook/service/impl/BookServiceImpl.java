package com.meng.fkbook.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meng.fkbook.dao.BookMapper;
import com.meng.fkbook.domain.Book;
import com.meng.fkbook.service.BookService;

@Service
public class BookServiceImpl implements BookService {

	@Autowired
	private BookMapper bookMapper;
	
	@Override
	public List<Book> getAll() {
		return bookMapper.findAll();
	}

}
