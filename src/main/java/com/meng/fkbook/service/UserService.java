package com.meng.fkbook.service;

import com.meng.fkbook.domain.User;

public interface UserService {

	User login(String loginname,String password);
}
