package com.meng.fkbook.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.meng.fkbook.domain.User;

public interface UserMapper {
	@Select("select * from tb_user where loginname=#{loginname} and password=#{password}")
	User findWithLoginNameAndPassword(@Param("loginname")String loginname,@Param("password")String password);
}
