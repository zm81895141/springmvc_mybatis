package com.meng.fkbook.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.meng.fkbook.domain.User;
import com.meng.fkbook.service.UserService;

@Controller
public class UserController {

	@Autowired
	private UserService userService;
	
	@RequestMapping("/login")
	public String login(String loginname,String password,HttpSession session) {
		User user  = userService.login(loginname, password); 
		if(user!=null) {
			session.setAttribute("user", user);
			return "redirect:main";
		}else {
			return "loginForm";
		}
	}
}
