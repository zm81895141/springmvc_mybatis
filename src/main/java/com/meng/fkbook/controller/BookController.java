package com.meng.fkbook.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.meng.fkbook.domain.Book;
import com.meng.fkbook.service.BookService;

@Controller
public class BookController {

	@Autowired
	private BookService bookService;
	
	@RequestMapping("/main")
	public String main(Model model) {
		List<Book> books = bookService.getAll();
		model.addAttribute("book_List", books);
		return "main";
	}
}
