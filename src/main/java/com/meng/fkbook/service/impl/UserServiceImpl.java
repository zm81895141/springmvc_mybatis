package com.meng.fkbook.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.meng.fkbook.dao.UserMapper;
import com.meng.fkbook.domain.User;
import com.meng.fkbook.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper usermapper;
	
	@Override
	public User login(String loginname, String password) {
		return usermapper.findWithLoginNameAndPassword(loginname, password);
	}

}
