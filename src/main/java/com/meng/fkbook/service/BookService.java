package com.meng.fkbook.service;

import java.util.List;

import com.meng.fkbook.domain.Book;

public interface BookService {

	List<Book> getAll();
}
