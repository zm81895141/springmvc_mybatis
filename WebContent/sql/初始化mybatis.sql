use mybatis;
#创建用户表
create table tb_user(
id INT PRIMARY KEY AUTO_INCREMENT,  #ID
loginname varchar(50) unique,       #登录名，邮箱
PASSWORD VARCHAR(18),				#密码
username varchar(18),				#用户名
phone varchar(18),					#电话
address varchar(255)				#地址
);

insert into tb_user(loginname,PASSWORD,username,phone,address) 
values ('jack','123456','杰克','13920001234','广州市天河区');


create table tb_book(
id int(11) primary key auto_increment,
name varchar(54),
author varchar(54),
publicationdate date,
publication varchar(150),
price double,
image varchar(54),
remark varchar(600)
);

insert into tb_book (id,name,author,publicationdate,publication,price,image,remark) 
values 
('1','疯狂JAVA讲义（附光盘）','李刚 编著','2018-10-10','电子工业出版社','74.2','java.jpg','疯狂源自梦想，技术成就辉煌'),
('2','轻量级JAVA EE企业应用实战','李刚 编著','2018-11-10','电子工业出版社','59.2','java.jpg','疯狂源自梦想，技术成就辉煌'),
('3','疯狂Android讲义（含CD光盘1张）','李刚 编著','2018-10-07','电子工业出版社','60.6','java.jpg','疯狂源自梦想，技术成就辉煌'),
('4','疯狂AJAX讲义（含CD光盘1张）','李刚 编著','2018-07-10','电子工业出版社','66.6','java.jpg','疯狂源自梦想，技术成就辉煌');