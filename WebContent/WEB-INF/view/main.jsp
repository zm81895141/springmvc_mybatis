<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<span>欢迎[${sessionScope.user.username}]访问</span>
<table>
<tr>
	<td>封面</td>
	<td>书名</td>
	<td>作者</td>
	<td>价格</td>
</tr>
<c:forEach items="${requestScope.book_List }" var="book">
	<tr>
		<td>${book.image}</td>
		<td>${book.name}</td>
		<td>${book.author}</td>
		<td>${book.price}</td>
	</tr>
</c:forEach>
</table>
</body>
</html>