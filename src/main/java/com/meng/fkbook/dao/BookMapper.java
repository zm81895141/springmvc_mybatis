package com.meng.fkbook.dao;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.meng.fkbook.domain.Book;

public interface BookMapper {
	
	@Select("select * from tb_book")
	List<Book> findAll();
}
